##
# batt-signal
#
# @file
# @version 0.1

PREFIX = /usr/local
SHAREPREFIX = $(PREFIX)/share/batt-signal

all: batt-signal

CFLAGS = -O2
CPPFLAGS = -DSHAREPREFIX=\"$(SHAREPREFIX)\"
LIBS = -L/usr/X11R6/lib -lX11 -lXxf86vm -lXft
INCS = -I/usr/include/freetype2

batt-signal: main.c
	gcc main.c -o batt-signal $(LIBS) $(INCS) $(CFLAGS) $(CPPFLAGS)

clean:
	rm -f batt-signal

install: all
	mkdir -p $(DESTDIR)$(PREFIX)/bin
	cp -f batt-signal $(DESTDIR)$(PREFIX)/bin

	mkdir -p $(DESTDIR)$(SHAREPREFIX)
	cp -f lobat.mp3 $(DESTDIR)$(SHAREPREFIX)

.PHONY: all install clean

# end
